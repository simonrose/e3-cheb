require chebyshev,master

epicsEnvSet(TOP, "$(E3_CMD_TOP)")

epicsEnvSet("IOCNAME", "ChebyshevIOC")

dbLoadRecords("$(chebyshev_DIR)/db/input.db", "IOCNAME=$(IOCNAME)")
dbLoadRecords("$(chebyshev_DIR)/db/coefficients.db", "IOCNAME=$(IOCNAME)")
dbLoadRecords("$(chebyshev_DIR)/db/cheb.db", "IOCNAME=$(IOCNAME)")


iocInit()
