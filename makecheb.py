import argparse
import sys
import operator
from functools import reduce

TEMPLATE_POLY = """record(calc, "$(IOCNAME):T_{degree}") {{
    field(DESC,"Chebyshev polynomial of degree {degree}")
    field(INPA,"$(IOCNAME):adjval PP MSS")
    field(INPB,"$(IOCNAME):COEFF_{degree}.VAL PP")
    field(CALC,"{formula}")
}}

"""

TEMPLATE_COEFF_RANGE = """record(ai, "$(IOCNAME):COEFF_{degree}_{range}") {{
    field(DESC,"Coeff. for Cheb poly, deg {degree}, range {range}")
    field(VAL, "{data}")
}}

"""

TEMPLATE_COEFF = """record(calc,"$(IOCNAME):COEFF_{degree}") {{
    field(INPA,"$(IOCNAME):log_value")
    field(INPB,"$(IOCNAME):break1.VAL PP")
    field(INPC,"$(IOCNAME):break2.VAL PP")
    field(INPD,"$(IOCNAME):COEFF_{degree}_low PP")
    field(INPE,"$(IOCNAME):COEFF_{degree}_mid PP")
    field(INPF,"$(IOCNAME):COEFF_{degree}_high PP")
    field(CALC,"(A < B)? D : ((A < C)?E:F)")
}}

"""

class ChebData(object):
    """
    Small class to store an manage the data to put in the .db file.
    """
    def __init__(self, keys:list, datafile:str):
        self.data = {}

        if datafile:
            try:
                with open(datafile,"r") as f:
                    for key, line in zip(keys, f.readlines()):
                        self.add(key,line.rstrip().split(','))
            except FileNotFoundError:
                pass
    
    def add(self, key:str, data:list):
        self.data[key] = data[:]

    def read(self, key:str, i:int):
        try:
            return self.data[key][i]
        except (KeyError, IndexError):
            return 0

def coeff(n:int, r:int):
    """
    This is the coefficient of x^{n - 2r} in the degree n Chebyshev polynomial.

    See https://en.wikipedia.org/wiki/Chebyshev_polynomials#Explicit_expressions
    """
    numer = reduce(operator.mul, range(r+1,n-r), n) * (-1) ** r * 2 ** (n - 2 * r)
    denom = reduce(operator.mul, range(1, n-2*r+1),2)
    return numer // denom

def monomial(var:str, degree:int):
    """
    Makes a monomial look prettier.
    """
    if degree == 0:
        return "1"
    elif degree == 1:
        return var
    else:
        return "{0}^{1}".format(var, degree)

def calc_poly(degree:int, variable="A", constant="B"):
    """
    Calculates the actual Chebyshev polynomial, outputs it in a way that is consistent
    with EPICS calc record.
    """
    if degree == 0:
        return constant
    elif degree == 1:
        return "{0}*{1}".format(constant,variable)
    else:
        terms = []
        for k in range(0, degree//2+1):
            terms.append("{0}*{1}".format(coeff(degree,k),monomial(variable,degree-2*k)))
        formula = terms.pop(0)
        for term in terms:
            if term[0] == '-':
                formula += term
            else:
                formula += '+' + term
        return "{0}*({1})".format(constant,formula)

def create_coeff_db(output_path: str, degree:int, datafile:str):
    """
    Creates the coefficient records needed to populate the DB file.
    Note: This probably should be done with a .template file instead.
    """
    ranges = ['low', 'mid', 'high']

    data = ChebData(ranges, datafile)

    out = ""
    for k in range(degree + 1):
        for r in ranges:
            out += TEMPLATE_COEFF_RANGE.format(degree=k, range=r, data=data.read(r,k))
        out += TEMPLATE_COEFF.format(degree=k)
    
    with open(output_path + 'coefficients.db', 'w') as f:
        f.write(out)

def create_poly_db(output_path:str, degree:int):
    """
    Creates the calculation records for each of the Chebyshev polynomials, as well as their sum.
    """
    out = ""
    for i in range(degree + 1):
        out += TEMPLATE_POLY.format(degree=i,formula=calc_poly(i))

    out += 'record(calc, "$(IOCNAME):CHEBAPPROX") {\n'
    s = "ABCDEFGHIJKL"
    for k in range(degree + 1):
        out += '    field(INP%s,"$(IOCNAME):T_%d PP MSS")\n' % (s[k],k)
    out += '    field(CALC,"%s")\n' % "+".join(map(lambda x : s[x],range(degree+1)))
    out += '}\n\n'

    with open(output_path + 'cheb.db', "w") as f:
        f.write(out)

def create_cheb_db(output_path:str, degree:int, datafile:str):
    create_coeff_db(output_path, degree, datafile)
    create_poly_db(output_path, degree)


def degree_type(x):
    x = int(x)
    if x > 11 or x < 0:
        raise argparse.ArgumentTypeError("Degree must be between 0 and 11.")
    return x

def main(args):
    parser = argparse.ArgumentParser(description="Creates .db file for Chebyshev polynomials")
    parser.add_argument("-n",help="Degree of polynomial",required=True,type=degree_type)
    parser.add_argument("-d",help="Input data file")
    parser.add_argument("output_path",help="Output file location")

    args = parser.parse_args(args)

    create_cheb_db(args.output_path, args.n, args.d)

if __name__=='__main__':
    main(sys.argv[1:])