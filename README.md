
e3-chebyshev
======
ESS Site-specific EPICS module : chebyshev

This module performs calculations with Chebyshev approximation. 

For use:
* Minimum/Maximum input values should be put in $(IOCNAME):value.HIHI and $(IOCNAME):value.LOLO
* The parameters $(IOCNAME):break$(n) set the breakpoints that distinguish between the low, middle, and high ranges of validity.
* The parameters $(IOCNAME):COEFF_$(d)_$(low/mid/high) sets the coefficients for the degree d Chebyshev polynomial in the low/mid/high range.
* Setting $(IOCNAME):value will result in the appropriate calculation, and the output will be $(IOCNAME):CHEBAPPROX.

Note that the cheb.db and coefficients.db files can be created with the python3 script makecheb.py and the data file data.csv

The data.csv file should actually come from the calibration service.